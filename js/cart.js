const decrease = id => {
    var count = document.getElementById("count-prod-" + id).innerHTML;
    count--;
    if (count > 0) {
        document.getElementById("count-prod-" + id).innerHTML = count;
    }
    else{
    	document.getElementById("prod-"+ id).remove();
    }
    setCookie(id, 'dec');
};

const increase = id => {
    var count = document.getElementById("count-prod-" + id).innerHTML;
    count++;
    document.getElementById("count-prod-" + id).innerHTML = count;
    setCookie(id, 'inc');
};

const setCookie = (id, cmd) => {
    var now = new Date();
    now.setTime(now.getTime() + 30 * 24 * 3600 * 1000);
    var expires = "expires="+now.toUTCString();
    
    if (cartCookiesJSON[id]!=undefined && cmd==='inc')
        cartCookiesJSON[id]++;
    else if (cartCookiesJSON[id]!=undefined && cmd==='dec')
        cartCookiesJSON[id]--;
    else
        cartCookiesJSON[id]=1;
    document.cookie = "cart="+JSON.stringify(cartCookiesJSON)+"; "+expires+"; path=/;";
};



$(document).ready( () => {
	if (document.cookie != "")
        cartCookiesJSON = JSON.parse(document.cookie.split(';')[0].trim().split('=')[1]);
    else
        cartCookiesJSON = {};


	for(prodId in cartCookiesJSON){
		if(cartCookiesJSON[prodId] > 0){
			var data = "\
				<div class='container cart-item' id='prod-"+prodId+"'>\
					<div class='col-xs-3'><img src='http://placehold.it/50x50' alt='' /></div>\
					<div class='col-xs-9'>\
						<h3 class='product-name'>Product</h3>\
						<h4 class='product-price'>Rs.99 <strike>Rs.100</strike></h4>\
						<div class='pull-right'>\
							<span class='glyphicon glyphicon-minus pull-left dec'></span>\
							<span id='count-prod-"+prodId+"'>"+cartCookiesJSON[prodId]+"</span>\
							<span class='glyphicon glyphicon-plus pull-right inc'></span>\
						</div>\
					</div>\
				</div>";
			$('#cart').append(data);
			$("#prod-" + prodId + " .dec").on("click", decrease.bind(this, prodId));
            $("#prod-" + prodId + " .inc").on("click", increase.bind(this, prodId));
		}
	}
});

