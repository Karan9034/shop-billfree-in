const decrease = id => {
    var count = document.getElementById("count-prod-" + id).innerHTML;
    count--;
    if (count > 0) {
        document.getElementById("count-prod-" + id).innerHTML = count;
    }
    else {
        document.getElementById("prod-"+id + "-btn").innerHTML = "Add to cart";
    }
    setCookie(id, 'dec');
};

const increase = id => {
    var count = document.getElementById("count-prod-" + id).innerHTML;
    count++;
    document.getElementById("count-prod-" + id).innerHTML = count;
    setCookie(id, 'inc');
};

const setCookie = (id, cmd) => {
    var now = new Date();
    now.setTime(now.getTime() + 30 * 24 * 3600 * 1000);
    var expires = "expires="+now.toUTCString();
    
    if (cartCookiesJSON[id]!=undefined && cmd==='inc')
        cartCookiesJSON[id]++;
    else if (cartCookiesJSON[id]!=undefined && cmd==='dec')
        cartCookiesJSON[id]--;
    else
        cartCookiesJSON[id]=1;
    document.cookie = "cart="+JSON.stringify(cartCookiesJSON)+"; "+expires+"; path=/;";
};

const setCartValue = (obj) => {
    for(prodId in obj){
        if (obj[prodId]>0){
            document.getElementById('prod-'+prodId+'-btn').innerHTML = "<span class='glyphicon glyphicon-minus pull-left dec'></span><span id='count-prod-" + prodId + "' style='font-weight: bold;'>"+ obj[prodId] +"</span><span class='glyphicon glyphicon-plus pull-right inc'></span>";          
        }
    }
};



$(document).ready(() => {
    if (document.cookie != "")
        cartCookiesJSON = JSON.parse(document.cookie.split(';')[0].trim().split('=')[1]);
    else
        cartCookiesJSON = {};
    setCartValue(cartCookiesJSON);


    for(prodId in cartCookiesJSON){
        if (cartCookiesJSON[prodId]>0){
            $("#prod-" + prodId + " .dec").on("click", decrease.bind(this, prodId));
            $("#prod-" + prodId + " .inc").on("click", increase.bind(this, prodId));
        }
    }

    $("div.btn").on("click", (e) => {
        var id = e.target.id.slice(0, -4);
        if(id.startsWith('prod')){
            prodId = id.slice(5);
            setCookie(prodId, 'inc');
            document.getElementById(e.target.id).innerHTML = "<span class='glyphicon glyphicon-minus pull-left dec'></span><span id='count-prod-" + prodId + "' style='font-weight: bold;'>"+cartCookiesJSON[prodId]+"</span><span class='glyphicon glyphicon-plus pull-right inc'></span>";
            $("#prod-" + prodId + " .dec").on("click", decrease.bind(this, prodId));
            $("#prod-" + prodId + " .inc").on("click", increase.bind(this, prodId));            
        }
    });
});



